import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import pandas as pd
from . import constants as cst
from scipy.spatial.transform import Rotation
import json
import os

def asSpherical(xyz):
    """Description:
Converts Cartesian coordinates to spherical coordinates.
- Input:
    - xyz - A list or array of Cartesian coordinates [x, y, z].
- Output: A list [r, theta, phi] where r is the radius, theta is the polar angle, and phi is the azimuthal angle.
"""
    x       = xyz[0]
    y       = xyz[1]
    z       = xyz[2]
    r       =  np.sqrt(x*x + y*y + z*z)
    theta   =  np.arccos(z/r)
    phi     =  np.arctan2(y,x)
    return [r,theta,phi]

def calculate_velocity(energy, mass):
    """Description:
Calculates the velocity of a particle given its energy and mass.
- Input:
  - energy - Total energy of the particle.
  - mass - Mass of the particle.
- Output: The velocity of the particle in meters per second.
"""
    velocity = np.sqrt(1-(mass**2 * cst.c**4/energy**2)) * cst.c
    return velocity

def calculate_trajectory(b_field, energy, charge, l):
    """Description:
Calculates the trajectory of a charged particle in a uniform magnetic field.
- Input:
  - b_field - Magnetic field vector.
  - energy - Energy of the particle.
  - charge - Charge of the particle.
  - l - Distance over which the trajectory is calculated.
- Output: Two lists, x and y, representing the x and y coordinates of the trajectory at each computed point."""
    position = np.array([0,0,0]) * u.m
    velocity_0 = calculate_velocity(energy, cst.e_mass).to_value()
    velocity = np.array([velocity_0, 0, 0]) * u.m / u.s
    R = ((energy/(cst.c*charge*b_field[2])).decompose())
    n = 100
    dt = (l/np.linalg.norm(velocity))/n
    x, y = [position[0].to_value()], [position[1].to_value()]
    v = np.linalg.norm(velocity)
    for i in range(n):
        velocity = u.Quantity([v * np.sin((v * dt * (i+1) / R).to(u.rad,equivalencies=u.dimensionless_angles())),
                               v * np.cos((v * dt * (i+1) / R).to(u.rad,equivalencies=u.dimensionless_angles())),
                               velocity[2]])
        position = u.Quantity([position[0] + velocity[1] * dt,
                               position[1] + velocity[0] * dt,
                               position[2]])
        x.append(position[0].to_value())
        y.append(position[1].to_value())
    return x, y

class Particle:
    """- Method: init
  - Initializes a particle with various properties.
- Method: propagate
  - Propagates the particle's position based on its velocity and distance to travel.
- Method: divide
  - Placeholder for particle division, to be overridden by subclasses."""
    def __init__(self, mass, energy, charge, pos_start, velocity, crit_energy, travel_dist) -> None:
        self.mass = mass
        self.energy = energy
        self.charge = charge
        self.position_start = pos_start
        self.velocity = velocity
        self.position_end = None
        self.crit_energy = crit_energy
        self.distance_travel = travel_dist
        self.color = None
    def propagate(self):
        if self.position_end == None:
            r, theta, phi = asSpherical(self.velocity)
            pos = np.array([0, 0, 0]) * u.m
            pos[0] = self.position_start[0] + np.sin(theta) * np.cos(phi) * self.distance_travel
            pos[1] = self.position_start[1] + np.sin(theta) * np.sin(phi) * self.distance_travel
            pos[2] = self.position_start[2] + np.cos(theta) * self.distance_travel
            self.position_end = pos
        return self.position_end

    def divide(self):
        pass
"""- Each subclass initializes specific particle types and implements the divide method for simulating particle interactions or decay processes."""
class Electron(Particle):
    def __init__(self, energy, pos_start, velocity) -> None:
        super().__init__(cst.e_mass, energy, cst.e_charge, pos_start, velocity, cst.crit_energy_e,  8.9 * np.log(2) * u.mm)
        self.color = "blue"
    def divide(self):
        rotaxis = np.array([np.random.uniform(-1, 1), np.random.uniform(-1, 1), np.random.uniform(-1, 1)])
        rotaxis = rotaxis / np.linalg.norm(rotaxis)
        theta = np.pi/4
        rot1 = Rotation.from_rotvec(theta * rotaxis)
        rot2 = Rotation.from_rotvec(-theta * rotaxis)
        part1 = Electron(self.energy/2, self.position_end, rot1.apply(self.velocity))
        part2 = Photon(self.energy/2, self.position_end, rot2.apply(self.velocity))
        return [part1, part2]

class Positron(Particle):
    def __init__(self, energy, pos_start, velocity) -> None:
        super().__init__(cst.e_mass, energy, -cst.e_charge, pos_start, velocity, cst.crit_energy_e,  8.9 * np.log(2) * u.mm)
        self.color = "red"
    def divide(self):
        rotaxis = np.array([np.random.uniform(-1, 1), np.random.uniform(-1, 1), np.random.uniform(-1, 1)])
        rotaxis = rotaxis / np.linalg.norm(rotaxis)
        theta = np.arcsin(cst.Rm/(np.ceil(np.log2(self.energy / cst.crit_energy_e)) * self.distance_travel))
        rot1 = Rotation.from_rotvec(theta * rotaxis)
        rot2 = Rotation.from_rotvec(-theta * rotaxis)
        part1 = Positron(self.energy/2, self.position_end, rot1.apply(self.velocity))
        part2 = Photon(self.energy/2, self.position_end, rot2.apply(self.velocity))
        return [part1, part2]

class Photon(Particle):
    def __init__(self, energy, pos_start, velocity) -> None:
        super().__init__(0, energy, 0, pos_start, velocity, cst.crit_energy_p,  8.9 * 7 / 9 * u.mm)
        self.color = "yellow"
    def divide(self):
        rotaxis = np.array([np.random.uniform(), np.random.uniform(), np.random.uniform()])
        rotaxis = rotaxis / np.linalg.norm(rotaxis)
        theta = np.pi/4
        rot1 = Rotation.from_rotvec(theta * rotaxis)
        rot2 = Rotation.from_rotvec(-theta * rotaxis)
        part1 = Electron(self.energy/2, self.position_end, rot1.apply(self.velocity))
        part2 = Positron(self.energy/2, self.position_end, rot2.apply(self.velocity))
        return [part1, part2]

def main_script(data, units):
    #test_relative_path = os.path.join("../../docs/examples", "test_yulia_1.json")
    #table = pd.read_json(test_relative_path)
    table = pd.DataFrame.from_dict(data)
    b_field = np.array([0, 0, 2])
    angles = []
    for particl in table.values:
        charge, energy, mass = particl
        x, y = calculate_trajectory(b_field* u.T, energy * u.GeV, charge * u.C, 1 * u.m)
        xl = x[-1]
        yl = y[-1]
        xp = x[-2]
        yp = y[-2]
        vx = xl - xp
        vy = yl - yp
        v = np.array([vx, vy])
        v = v/np.linalg.norm(v)
        angle = -np.arctan2(v[1], v[0])
        angles.append(angle)
    table["angles"] = angles
    box_size = 5 * u.cm
    particles = []
    for particle in table.values:
        charge, energy, mass, angle = particle
        vel = calculate_velocity(energy * u.GeV, mass * u.kg)
        p1 = Electron(energy * u.GeV, np.array([0, 0, 0]) * u.m, vel * np.array([np.cos(angle), np.sin(angle), 0]))
        got_particles = [p1]
        while got_particles:
            current_particle = got_particles.pop(0)
            particles.append(current_particle)
            new_pos = current_particle.propagate()
            if current_particle.energy > current_particle.crit_energy * 2 and (0 * u.cm <= current_particle.position_end[0] <= box_size) and (-box_size <= (current_particle.position_end[1]) <= 0 * u.cm):
                new_particles = current_particle.divide()
                got_particles.extend(new_particles)
    plt.figure().add_subplot(projection='3d')
    print(len(particles))
    for p in particles:
        pst = p.position_start.value
        pend = p.position_end.value
        x = np.array([pst[0], pend[0]])
        y = np.array([pst[1], pend[1]])
        z = np.array([pst[2], pend[2]])
        #plt.plot(x, y, z, color = p.color)
    mini_box_size = 1 * u.mm
    N = int((box_size // mini_box_size).value)
    energies = np.zeros((N, N, N)) * u.GeV
    for p in particles:
        final_pos = p.position_end
        nx = int((final_pos[0]//mini_box_size).value)
        ny = -int((final_pos[1]//mini_box_size).value)
        nz = int((final_pos[2]//mini_box_size).value)
        if 0 < nx < N and 0<ny < N and 0 < nz < N:
            energies[nx, ny, nz] += p.energy
    #plt.matshow(energies.value)
    energies2 = energies.value.tolist()
    results = {
        "box_size": box_size.value,
        "mini_box_size" : mini_box_size.to(u.cm).value,
        "values" : energies2
    }
    #print(energies2[0, 0])
    with open("results.json", "w") as r:
        json.dump(results, r)
    return results
