#!/usr/bin/env python3
import argparse
import json
import astropy.units as u
import numpy as np
from . import constants as c


def get_parser():
    """Function to create a parser.

    Returns
    -------
    parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--json-file', type=str, help='Path to the configuration file')
    return parser

def read(json_file):
    """
    Function to convert JSON file to python dictionary.

    Parameters
    ----------
    json_file : str
        Path to the configuration file

    Returns
    -------
    config : dict
        Configuration as a python dictionary
    """
    with open(json_file, 'r') as j_file:
        config = json.load(j_file)
        print(f'\nConfig loaded from {json_file}:')
        print(config)
        return config


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    config = read(args.json_file)


def get_data(config):
    """
    Function to get data from configuration.

    Parameters
    ----------
    config : dict
        Configuration as a python dictionary

    Returns
    -------
    energies : float
        particles energies
    units : str
        energy unit
    num : int
        number of particles
    masses : float
        particles masses
    charges : float
        particles charges
    """
    particle = config["type"]
    energies = config["energies"]["values"]
    units = config["energies"]["units"]
    num = config["number"]
    if particle == "electron":
        charge = c.e_charge
        mass = c.e_mass
    elif particle == "positron":
        charge = c.p_charge
        mass = c.e_mass
    masses = u.Quantity([mass for i in range(num)])
    charges = u.Quantity([charge for i in range(num)])
    return energies, units, num, masses, charges


def constant_energy(config):
    """
    Function to create particles with equal energies.

    Parameters
    ----------
    config : dict
        Configuration as a python dictionary

    Returns
    -------
    result : dict
        Dictionary of parameters, such as charges, masses and energies of particles
    """
    energies, units, num, masses, charges = get_data(config)
    const_energies = [energies[0] for i in range(num)]
    res_energies = u.Quantity(const_energies, unit=u.Unit(units))
    result = {"charge": charges, "energy": res_energies, "mass": masses}
    data = {key: value.value.tolist() for key, value in result.items()}
    print(f"\nResult:\n{data}")
    with open("generated_particles.json", "w") as r:
        json.dump(data, r)
    return data, units


def linear_growth(config):
    """
    Function to create particles with linearly increasing energies.

    Parameters
    ----------
    config : dict
        Configuration as a python dictionary

    Returns
    -------
    result : dict
        Dictionary of parameters, such as charges, masses and energies of particles
    """
    energies, units, num, masses, charges = get_data(config)
    lin_energies = [round(energies[0] + (energies[1] - energies[0]) * i / (num-1), 2) for i in range(num)]
    res_energies = u.Quantity(lin_energies, unit=u.Unit(units))
    result = {"charge": charges, "energy": res_energies, "mass": masses}
    data = {key: value.value.tolist() for key, value in result.items()}
    print(f"\nResult:\n{data}")
    with open("generated_particles.json", "w") as r:
        json.dump(data, r)
    return data, units


def exponential_growth(config):
    """
    Function to create particles with exponentially increasing energies.

    Parameters
    ----------
    config : dict
        Configuration as a python dictionary

    Returns
    -------
    result : dict
        Dictionary of parameters, such as charges, masses and energies of particles
    """
    energies, units, num, masses, charges = get_data(config)
    log_energies = np.linspace(np.log(energies[0]), np.log(energies[1]), num)
    exp_energies = [round(i, 2) for i in np.exp(log_energies)]
    res_energies = u.Quantity(exp_energies, unit=u.Unit(units))
    result = {"charge": charges, "energy": res_energies, "mass": masses}
    data = {key: value.value.tolist() for key, value in result.items()}
    print(f"\nResult:\n{data}")
    with open("generated_particles.json", "w") as r:
        json.dump(data, r)
    return data, units
