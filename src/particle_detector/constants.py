import astropy.units as u
from astropy.units import cds
from astropy.constants import c, m_e, m_p, e

p_mass = m_p
p_charge = e.si

e_mass = m_e
e_charge = -1 * p_charge

c = c

crit_energy_e = 9.64 * u.MeV
crit_energy_p = 9.31 * u.MeV

Rm = 2.2 * u.cm
