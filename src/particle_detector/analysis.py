import numpy as np
import astropy.units as u
import pandas as pd
import json
import os

def get_test_array(data=None):
    """
    Function to get the test array.

    Returns
    ----------
    test_array : pandas.core.series.Series
        Array which is analyzed
    """
    results_relative_path = os.path.join("../../../docs/examples", "results.json")
    #n = 2
    #test_array = np.zeros((n, n, n))
    if data:
        print("Taking data from a parameter")
        result = pd.DataFrame.from_dict(data)
    else:
        print("Reading data from a test file")
        result = pd.read_json(results_relative_path)
    array = result["values"]
    #for i in range(n):
    #    test_array[i] = array[i]
    #return test_array
    return array

def initialize_patterns(patterns_path):
    """
    Function to initialize patterns array.

    Returns
    ----------
    patterns : numpy.ndarray
        Array with patterns
    patterns_key : list
        List with corresponding particle names
    patterns_energy : astropy.units.quantity.Quantity
        List with corresponding energies
    pattern_quantity : int
        Number of patterns
    """
    #pattern_relative_path = os.path.join("../../../docs/examples", "patterns.json")
    n = 49
    pattern_quantity = 1
    patterns = np.zeros((pattern_quantity, n, n, n))
    patterns_key = [""]
    patterns_energy = np.zeros(pattern_quantity) * u.GeV
    #reader = pd.read_json(pattern_relative_path)
    with open(patterns_path, 'r') as ifile:
        reader = json.load(ifile)
    reader_pattern_array = np.asarray(reader["values"])
    reader_pattern_key = np.asarray(reader["key"])
    reader_pattern_energy = np.asarray(reader["energy"])
    for i in range(pattern_quantity):
        patterns[i] = reader_pattern_array[i]
        patterns_key[i] = reader_pattern_key[i]
        patterns_energy[i] = reader_pattern_energy[i] * u.GeV
    return patterns, patterns_key, patterns_energy, pattern_quantity

def analysis(test_array, patterns, patterns_key, patterns_energy, pattern_quantity):
    """
    Function to analyze if the array corresponds to a pattern.

    Parameters
    ----------
    test_array : numpy.ndarray
        Array which is analyzed
    patterns : numpy.ndarray
        Array with patterns
    patterns_key : list
        List with corresponding particle names
    patterns_energy : astropy.units.quantity.Quantity
        List with corresponding energies
    pattern_quantity : int
        Number of patterns

    Returns
    ----------
    particle_type : str
        Type of the particle
    energy : astropy.units.quantity.Quantity
        Energy of the particle
    """
    for i in range(pattern_quantity):
        print(type(patterns[i]))
        print(patterns[i].shape)
        print(test_array.shape)
        if(np.all(patterns[i][:,0,0] == test_array)):
            particle_type = patterns_key[i]
            energy = patterns_energy[i]
            return particle_type, energy
    particle_type = "Unknown particle"
    energy = 0
    return particle_type, energy
