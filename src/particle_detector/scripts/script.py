import argparse
from enum import Enum
import astropy.units as u
from .. import particle_generator
from .. import detector
from .. import analysis


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--json-file', type=str, help='Path to the configuration file')
    parser.add_argument('-s', '--spectrum-type', help='Name of the spectrums')
    parser.add_argument('-p', '--patterns', help='Path to the analysis patterns file')
    return parser


class SpectrumEnum(Enum):
    CONSTANT = particle_generator.constant_energy
    LINEAR = particle_generator.linear_growth
    EXPONENTIAL = particle_generator.exponential_growth


def main():
    parser = get_parser()
    args = parser.parse_args()
    config = particle_generator.read(args.json_file)
    func = getattr(SpectrumEnum, args.spectrum_type, None)
    data, units = func(config)
    detected_particles = detector.main_script(data, units)
    patterns, patterns_key, patterns_energy, pattern_quantity = analysis.initialize_patterns(args.patterns)
    test_array = analysis.get_test_array(detected_particles)
    pid, energy = analyzed_particles = analysis.analysis(
        test_array,
        patterns,
        patterns_key,
        patterns_energy,
        pattern_quantity
    )
    print(f"Reconstructed particle type: {pid}\nReconstructed energy: {energy}")

if __name__ == "__main__":
    main()

